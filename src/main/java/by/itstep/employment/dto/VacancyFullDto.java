package by.itstep.employment.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class VacancyFullDto {

    private Integer id;
    private String name;
    private String position;
    private Integer salary;
    private String companyName;
    private List<InterviewShortDto> interviews = new ArrayList<>();
}
