package by.itstep.employment.dto;

import lombok.Data;

import java.sql.Date;


@Data
public class InterviewFullDto {

    private Integer interviewId;

    private String status;

    private Date date;

    private UserShortDto user;

    private VacancyShortDto vacancy;
}
