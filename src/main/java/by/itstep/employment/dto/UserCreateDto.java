package by.itstep.employment.dto;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class UserCreateDto {

    @NotBlank(message = "Name can ne be blank!")
    private String firstName;

    @NotBlank(message = "Name can ne be blank!")
    private String lastName;

    @Email(message = "Email must have valid format")
    @NotNull(message = "Email can not be null")
    private String email;

    @NotBlank(message = "Password can not be blank!")
    @Size(min = 8, max = 100,message = "Password length must be between 8 and 100 ")
    private String password;

}
