package by.itstep.employment.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class UserFullDto {

    private Integer userId;
    private String firstName;
    private String lastName;
    private String email;
    private List<InterviewShortDto> interviews = new ArrayList<>();
}
