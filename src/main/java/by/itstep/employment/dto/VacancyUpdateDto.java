package by.itstep.employment.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class VacancyUpdateDto {

    @NotNull
    private Integer id;

    @NotBlank(message = "Name can ne be blank!")
    private String name;

    @NotBlank(message = "Position can ne be blank!")
    private String position;

    @NotNull
    private Integer salary;

    @NotBlank(message = "CompanyName can ne be blank!")
    private String companyName;
}
