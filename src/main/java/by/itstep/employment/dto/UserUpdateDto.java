package by.itstep.employment.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class UserUpdateDto {

    @NotNull
    private Integer userId;

    @NotBlank(message = "Name can ne be blank!")
    private String firstName;

    @NotBlank(message = "Name can ne be blank!")
    private String lastName;

}
