package by.itstep.employment.dto;

import lombok.Data;

@Data
public class VacancyShortDto {

    private Integer id;
    private String name;
    private String position;
    private Integer salary;
    private String companyName;
}
