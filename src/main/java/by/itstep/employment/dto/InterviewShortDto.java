package by.itstep.employment.dto;

import lombok.Data;

import java.sql.Date;

@Data
public class InterviewShortDto {

    private Integer interviewId;

    private String status;

    private Date date;

}
