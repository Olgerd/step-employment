package by.itstep.employment.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class VacancyCreateDto {

    @NotBlank(message = "Name can ne be blank!")
    private String name;

    @NotBlank(message = "Position can ne be blank!")
    private String position;

    @NotNull(message = "Salary can not be null")
    private Integer salary;

    @NotBlank(message = "CompanyName can ne be blank!")
    private String companyName;

}
