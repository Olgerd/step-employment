package by.itstep.employment.dto;

import lombok.Data;

import java.sql.Date;

@Data
public class InterviewUpdateDto {

    private Integer interviewId;

    private String status;

    private Date date;

    private Integer userId;

    private Integer vacancyId;


}
