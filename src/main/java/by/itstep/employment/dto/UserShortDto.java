package by.itstep.employment.dto;

import lombok.Data;

@Data
public class UserShortDto {

    private Integer userId;
    private String firstName;
    private String lastName;
    private String email;
}
