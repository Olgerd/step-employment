package by.itstep.employment.dto;


import lombok.Data;

import java.sql.Date;

@Data
public class InterviewCreateDto {

    private String status;

    private Date date;

    private Integer userId;

    private Integer vacancyId;


}
