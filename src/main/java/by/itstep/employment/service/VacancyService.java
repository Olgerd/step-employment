package by.itstep.employment.service;


import by.itstep.employment.dto.VacancyCreateDto;
import by.itstep.employment.dto.VacancyFullDto;
import by.itstep.employment.dto.VacancyShortDto;
import by.itstep.employment.dto.VacancyUpdateDto;
import by.itstep.employment.entity.InterviewEntity;
import by.itstep.employment.entity.UserEntity;
import by.itstep.employment.entity.VacancyEntity;
import by.itstep.employment.mapper.VacancyMapper;
import by.itstep.employment.repository.InterviewRepository;
import by.itstep.employment.repository.UserRepository;
import by.itstep.employment.repository.VacancyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class VacancyService {

    @Autowired
    private VacancyRepository vacancyRepository;
    @Autowired
    private InterviewRepository interviewRepository;
    @Autowired
    private VacancyMapper vacancyMapper;

    public VacancyFullDto findById(int id){
        VacancyEntity vacancy = vacancyRepository.findById(id);
        if(vacancy==null){
            throw new RuntimeException("VacancyService ->Vacancy not found by id: "+ id);
        }
        System.out.println("VacancyService -> Found vacancy: "+ vacancy);

        VacancyFullDto dto = vacancyMapper.map(vacancy);
        return dto;
    }

    public List<VacancyShortDto> findAll(){
        List<VacancyEntity> allVacancys = vacancyRepository.findAll();
        System.out.println("VacancyService -> Found vacancys: "+ allVacancys);

        List<VacancyShortDto> allDtos = vacancyMapper.map(allVacancys);
        return allDtos;
    }

    public VacancyFullDto create(VacancyCreateDto createRequest){

        VacancyEntity vacancy = vacancyMapper.map(createRequest);
        if(vacancy.getVacancyId()!=null){
            throw new RuntimeException("VacancyService -> Can not create vacancy with id.");
        }
        VacancyEntity createdVacancy = vacancyRepository.create(vacancy);
        System.out.println("VacancyService -> Created vacancy: "+ vacancy);


        VacancyFullDto createdDto = vacancyMapper.map(createdVacancy);
        return  createdDto;
    }


    public VacancyFullDto update(VacancyUpdateDto updateRequest){

        if(updateRequest.getId()==null){
            throw new RuntimeException("VacancyService -> Can not update entity without id");
        }
        if(vacancyRepository.findById(updateRequest.getId())==null){
            throw new RuntimeException("VacancyService -> Vacancy not found by id:"+ updateRequest.getId());
        }

        VacancyEntity vacancy = vacancyRepository.findById(updateRequest.getId());
        vacancy.setName(updateRequest.getName());
        vacancy.setPosition(updateRequest.getPosition());
        vacancy.setSalary(updateRequest.getSalary());
        vacancy.setCompanyName(updateRequest.getCompanyName());

        VacancyEntity updatedVacancy = vacancyRepository.update(vacancy);
        System.out.println("VacancyService -> Updated vacancy: "+ updateRequest);
        VacancyFullDto updatedDto = vacancyMapper.map(updatedVacancy);

        return updatedDto;
    }


    public void deleteById(int id){
        VacancyEntity vacancyToDelete = vacancyRepository.findById(id);
        if(vacancyToDelete==null){
            throw new RuntimeException("VacancyService -> Vacancy wasn't found by id: "+id+
                    " and can not be deleted.");
        }
        List<InterviewEntity> existingInterviews = vacancyToDelete.getInterviews();
        for(InterviewEntity i: existingInterviews){
            interviewRepository.deleteById(i.getInterviewId());
        }
        vacancyRepository.deleteById(id);
        System.out.println("VacancyService -> Deleted vacancy: " + vacancyToDelete);
    }

}
