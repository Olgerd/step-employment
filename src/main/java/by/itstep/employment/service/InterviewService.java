package by.itstep.employment.service;


import by.itstep.employment.dto.InterviewCreateDto;
import by.itstep.employment.dto.InterviewFullDto;
import by.itstep.employment.dto.InterviewShortDto;
import by.itstep.employment.dto.InterviewUpdateDto;
import by.itstep.employment.entity.InterviewEntity;
import by.itstep.employment.entity.UserEntity;
import by.itstep.employment.entity.VacancyEntity;
import by.itstep.employment.mapper.InterviewMapper;
import by.itstep.employment.repository.InterviewRepository;
import by.itstep.employment.repository.UserRepository;
import by.itstep.employment.repository.VacancyRepository;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;

@Service
public class InterviewService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private InterviewRepository interviewRepository;
    @Autowired
    private VacancyRepository vacancyRepository;
    @Autowired
    private InterviewMapper interviewMapper;


    public InterviewFullDto findById(Integer interviewId){
        InterviewEntity interview = interviewRepository.findById(interviewId);
        if(interview==null){

            throw new RuntimeException("InterviewService ->Interview not found by id: "+ interviewId);
        }
        System.out.println("InterviewService -> Found interview: "+ interview);

        InterviewFullDto dto = interviewMapper.map(interview);
        return dto;

    }

    public InterviewFullDto create(InterviewCreateDto createRequest){
        InterviewEntity interview = interviewMapper.map(createRequest);
        if(createRequest.getUserId() == null || createRequest.getVacancyId() == null){
            throw new RuntimeException("InterviewService -> Can not create interview with ?????? id.");
        }
        List<InterviewEntity> existingInterviews = interviewRepository.findAll();

        //TODO:
        //  1. Найти юзера и вакансию испольщуя  Integer userId, Integer vacancyId
        UserEntity user = userRepository.findById(createRequest.getUserId());
        VacancyEntity vacancy = vacancyRepository.findById(createRequest.getVacancyId());

        //  2. Проверить что они точно были найдены

        List<UserEntity> existingUsers = userRepository.findAll();
        for(UserEntity u: existingUsers){
            if(u.getEmail().equalsIgnoreCase(user.getEmail())){
                throw new RuntimeException("UserService -> Email: "+ user.getEmail()+" is taken!");
            }
        }

        List<VacancyEntity> existingVacancys = vacancyRepository.findAll();
        for(VacancyEntity u: existingVacancys){
            if(u.getName().equalsIgnoreCase(vacancy.getName())){
                throw new RuntimeException("VacancyService -> Name: "+ vacancy.getName()+" is taken!");
            }
        }

        // List< InterviewEntity> interview = interviewRepository.findAll();
       interview.setUser(user);
        interview.setVacancy(vacancy);
        interview.setStatus(interview.getStatus());
        //  4. Не забыть указать СЕГОДНЯШНЮЮ дату и СТАТУС по умолчанию
        interview.setDate(new Date(22-01-02));
        InterviewEntity createdInterview = interviewRepository.create(interview);
        System.out.println("InterviewService -> Created interview: "+ interview);

        InterviewFullDto dto = interviewMapper.map(createdInterview);
        return  dto;
    }
    public InterviewFullDto updateStatus(InterviewUpdateDto updateRequest){
        // TODO -> Найти интервью в БАЗЕ используя Integer interviewId
        InterviewEntity interview = interviewRepository.findById(updateRequest.getInterviewId());

        if(interview.getInterviewId()==null){
            throw new RuntimeException("InterviewService -> Interview not found by id:"+ interview.getInterviewId());
        }

        // TODO -> В найденном интервью поменять статус НА String status
        interview.setStatus(updateRequest.getStatus());
        InterviewEntity updatedInterview = interviewRepository.update(interview);
        System.out.println("InterviewService -> Updated interview: "+ interview);

        InterviewFullDto dto = interviewMapper.map(updatedInterview);
        return dto;

    }



    public List<InterviewShortDto> findAll(){
        List<InterviewEntity> allInterviews = interviewRepository.findAll();

        System.out.println("InterviewService -> Found interviews: "+ allInterviews);
        List<InterviewShortDto> allDtos = interviewMapper.map(allInterviews);

        return allDtos;
    }
}
