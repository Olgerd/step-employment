package by.itstep.employment.service;

import by.itstep.employment.config.SecurityService;
import by.itstep.employment.dto.UserCreateDto;
import by.itstep.employment.dto.UserFullDto;
import by.itstep.employment.dto.UserShortDto;
import by.itstep.employment.dto.UserUpdateDto;
import by.itstep.employment.entity.InterviewEntity;
import by.itstep.employment.entity.UserEntity;
import by.itstep.employment.mapper.UserMapper;
import by.itstep.employment.repository.InterviewRepository;
import by.itstep.employment.repository.UserRepository;
import by.itstep.employment.repository.VacancyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private VacancyRepository vacancyRepository;
    @Autowired
    private InterviewRepository interviewRepository;
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private SecurityService securityService;

    public UserFullDto findById(int id){
        UserEntity user = userRepository.findById(id);
        if(user==null){
            throw new RuntimeException("UserService ->User not found by id: "+ id);
        }
        System.out.println("UserService -> Found user: "+ user);

        UserFullDto dto =userMapper.map(user);
        return dto;
    }

    public List<UserShortDto> findAll(){
        List<UserEntity> allUsers = userRepository.findAll();
        System.out.println("UserService -> Found users: "+ allUsers);
        List<UserShortDto> allDtos =userMapper.map(allUsers);


        return allDtos;
    }

    public UserFullDto create(UserCreateDto createRequest){

        UserEntity user = userMapper.map(createRequest);
        if(user.getUserId()!=null){
            throw new RuntimeException("UserService -> Can not create user with id.");
        }
        List<UserEntity> existingUsers = userRepository.findAll();
        for(UserEntity u: existingUsers){
            if(u.getEmail().equalsIgnoreCase(user.getEmail())){
                throw new RuntimeException("UserService -> Email: "+ user.getEmail()+" is taken!");
            }
        }
        UserEntity createdUser = userRepository.create(user);
        System.out.println("UserService -> Created user: "+ user);
        UserFullDto createdDto = userMapper.map(createdUser);


        return  createdDto;
    }


    public UserFullDto update(UserUpdateDto updateRequest){
        UserEntity currentUser = securityService.getAuthenticatedUser();
        if(!currentUser.getUserId().equals(updateRequest.getUserId())){

            throw  new RuntimeException("UserService -> Can't update another user!");
        }

        if(updateRequest.getUserId()==null){
            throw new RuntimeException("UserService -> Can not update entity without id");
        }
        if(userRepository.findById(updateRequest.getUserId())==null){
            throw new RuntimeException("UserService -> User not found by id:"+ updateRequest.getUserId());
        }

        UserEntity user = userRepository.findById(updateRequest.getUserId());
        user.setFirstName(updateRequest.getFirstName());
        user.setLastName(updateRequest.getLastName());

        UserEntity updatedUser = userRepository.update(user);
        System.out.println("UserService -> Updated user: "+ updateRequest);
        UserFullDto updatedDto = userMapper.map(updatedUser);

        return updatedDto;
    }


    public void deleteById(int id){
        UserEntity userToDelete = userRepository.findById(id);
        if(userToDelete==null){
            throw new RuntimeException("UserService -> User wasn't found by id: "+id+
                    " and can not be deleted.");
        }

        List<InterviewEntity> existingInterviews = userToDelete.getInterviews();

        for(InterviewEntity c: existingInterviews){
            interviewRepository.deleteById(c.getInterviewId());
        }

        userRepository.deleteById(id);
        System.out.println("UserService -> Deleted user: "+ userToDelete);
    }


}
