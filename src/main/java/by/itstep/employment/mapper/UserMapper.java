package by.itstep.employment.mapper;

import by.itstep.employment.dto.InterviewShortDto;
import by.itstep.employment.dto.UserCreateDto;
import by.itstep.employment.dto.UserFullDto;
import by.itstep.employment.dto.UserShortDto;
import by.itstep.employment.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Component
public class UserMapper {

    @Autowired
    private InterviewMapper interviewMapper;

    public UserFullDto map(UserEntity entity){

        UserFullDto dto = new UserFullDto();
        dto.setUserId(entity.getUserId());
        dto.setEmail(entity.getEmail());
        dto.setFirstName(entity.getFirstName());
        dto.setLastName(entity.getLastName());
        List<InterviewShortDto> interviewDtos = interviewMapper
                .map(entity.getInterviews());

        dto.setInterviews(interviewDtos);
        return dto;
    }
    public List<UserShortDto> map(List<UserEntity> entities){
        List<UserShortDto> dtos = new ArrayList<>();
        for (UserEntity entity : entities){
            UserShortDto dto = new UserShortDto();
            dto.setUserId(entity.getUserId());
            dto.setEmail(entity.getEmail());
            dto.setFirstName(entity.getFirstName());
            dto.setLastName(entity.getLastName());

            dtos.add(dto);
        }
        return dtos;

    }
    public UserEntity map(UserCreateDto dto){

        UserEntity entity = new UserEntity();

        entity.setFirstName(dto.getFirstName());
        entity.setLastName(dto.getLastName());
        entity.setEmail(dto.getEmail());
        entity.setPassword(dto.getPassword());

        return entity;
    }

    public UserShortDto mapToShort(UserEntity entity){

      UserShortDto dto = new UserShortDto();
      dto.setUserId(entity.getUserId());
      dto.setFirstName(entity.getFirstName());
      dto.setLastName(entity.getLastName());
      dto.setEmail(entity.getEmail());

      return dto;
    }
}
