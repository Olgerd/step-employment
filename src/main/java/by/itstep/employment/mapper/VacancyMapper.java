package by.itstep.employment.mapper;

import by.itstep.employment.dto.InterviewShortDto;
import by.itstep.employment.dto.VacancyCreateDto;
import by.itstep.employment.dto.VacancyFullDto;
import by.itstep.employment.dto.VacancyShortDto;
import by.itstep.employment.entity.VacancyEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class VacancyMapper {

    @Autowired
    private InterviewMapper interviewMapper;

    public VacancyFullDto map(VacancyEntity entity){

        VacancyFullDto dto = new VacancyFullDto();
        dto.setId(entity.getVacancyId());
        dto.setName(entity.getName());
        dto.setPosition(entity.getPosition());
        dto.setSalary(entity.getSalary());
        dto.setCompanyName(entity.getCompanyName());
        List<InterviewShortDto> interviewDtos = interviewMapper.map(entity.getInterviews());

        dto.setInterviews(interviewDtos);
        return dto;
    }

    public List<VacancyShortDto> map(List<VacancyEntity> entities) {
        List<VacancyShortDto> dtos = new ArrayList<>();
        for (VacancyEntity entity : entities){
            VacancyShortDto dto = new VacancyShortDto();
            dto.setId(entity.getVacancyId());
            dto.setName(entity.getName());
            dto.setPosition(entity.getPosition());
            dto.setSalary(entity.getSalary());
            dto.setCompanyName(entity.getCompanyName());

            dtos.add(dto);
        }
        return dtos;
    }
    public VacancyEntity map(VacancyCreateDto dto){

        VacancyEntity entity = new VacancyEntity();

        entity.setName(dto.getName());
        entity.setPosition(dto.getPosition());
        entity.setSalary(dto.getSalary());
        entity.setCompanyName(dto.getCompanyName());

        return entity;
    }
    public VacancyShortDto mapToShort(VacancyEntity entity){

       VacancyShortDto dto = new VacancyShortDto();
       dto.setId(entity.getVacancyId());
       dto.setName(entity.getName());
       dto.setSalary(entity.getSalary());
       dto.setPosition(entity.getPosition());
       dto.setCompanyName(entity.getCompanyName());

       return  dto;
    }


}
