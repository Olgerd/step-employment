package by.itstep.employment.mapper;

import by.itstep.employment.dto.*;
import by.itstep.employment.entity.InterviewEntity;
import by.itstep.employment.entity.UserEntity;
import by.itstep.employment.entity.VacancyEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class InterviewMapper {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private VacancyMapper vacancyMapper;

    public List<InterviewShortDto> map(List<InterviewEntity> entities) {
        List<InterviewShortDto> dtos = new ArrayList<>();
        for (InterviewEntity entity : entities) {
            InterviewShortDto dto = new InterviewShortDto();
            dto.setInterviewId(entity.getInterviewId());
            dto.setStatus(entity.getStatus());
            dto.setDate(entity.getDate());

            dtos.add(dto);
        }
        return dtos;
    }

    public InterviewFullDto map(InterviewEntity interview){

        InterviewFullDto dto = new InterviewFullDto();
        dto.setInterviewId(interview.getInterviewId());
        dto.setDate(interview.getDate());
        dto.setStatus(interview.getStatus());

        UserShortDto userDto = userMapper.mapToShort(interview.getUser());
        VacancyShortDto vacancyDto = vacancyMapper.mapToShort(interview.getVacancy());

        dto.setUser(userDto);
        dto.setVacancy(vacancyDto);

        return dto;

    }

    public InterviewEntity map(InterviewCreateDto interviewDto){

      InterviewEntity entity = new InterviewEntity();
       entity.setInterviewId(interviewDto.getUserId());
       entity.setDate(interviewDto.getDate());
       entity.setStatus(interviewDto.getStatus());

       return entity;
    }

    public InterviewEntity map(InterviewFullDto interviewFullDto){
        InterviewEntity entity = new InterviewEntity();
        entity.setInterviewId(interviewFullDto.getInterviewId());
        entity.setDate(interviewFullDto.getDate());
        entity.setStatus(interviewFullDto.getStatus());


        return entity;


    }
}
