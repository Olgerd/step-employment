package by.itstep.employment.controller;

import by.itstep.employment.dto.UserCreateDto;
import by.itstep.employment.dto.UserFullDto;
import by.itstep.employment.dto.UserShortDto;
import by.itstep.employment.dto.UserUpdateDto;
import by.itstep.employment.entity.UserEntity;
import by.itstep.employment.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;



@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @ResponseBody
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public List<UserShortDto> findAllUsers() {
        List<UserShortDto> allUsers = userService.findAll();
        return allUsers;
    }

    @ResponseBody
    @RequestMapping(value = "/users/{userId}", method = RequestMethod.GET)
    public UserFullDto findById(@PathVariable Integer id) {
        UserFullDto user = userService.findById(id);
        return user;
    }

    @ResponseBody
    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public UserFullDto create(@RequestBody UserCreateDto createRequest) {
        UserFullDto createdUser = userService.create(createRequest);
        return createdUser;
    }

    @ResponseBody
    @RequestMapping(value = "/users", method = RequestMethod.PUT)
    public UserFullDto update(@RequestBody UserUpdateDto updateRequest) {
        UserFullDto updatedUser = userService.update(updateRequest);
        return updatedUser;
    }

    @ResponseBody
    @RequestMapping(value = "/users/{userId}", method = RequestMethod.DELETE)
    public void delete(@PathVariable int id){
        userService.deleteById(id);
    }
}
