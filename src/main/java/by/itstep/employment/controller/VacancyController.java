package by.itstep.employment.controller;


import by.itstep.employment.dto.VacancyCreateDto;
import by.itstep.employment.dto.VacancyFullDto;
import by.itstep.employment.dto.VacancyShortDto;
import by.itstep.employment.dto.VacancyUpdateDto;
import by.itstep.employment.entity.UserEntity;
import by.itstep.employment.entity.VacancyEntity;
import by.itstep.employment.service.UserService;
import by.itstep.employment.service.VacancyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class VacancyController {

    @Autowired
    private VacancyService vacancyService;

    @ResponseBody
    @RequestMapping(value = "/vacancys", method = RequestMethod.GET)
    public List<VacancyShortDto> findAll(){
        List<VacancyShortDto> allVacancys = vacancyService.findAll();
        return allVacancys;

    }

    // vacancies
    @ResponseBody
    @RequestMapping(value = "/vacancys/{vacancyId}", method = RequestMethod.GET)
    public VacancyFullDto findById(@PathVariable Integer id) {
        VacancyFullDto vacancy = vacancyService.findById(id);
        return vacancy;
    }

    @ResponseBody
    @RequestMapping(value = "/vacancys", method = RequestMethod.POST)
    public VacancyFullDto create(@RequestBody VacancyCreateDto createRequest) {
        VacancyFullDto createdVacancy = vacancyService.create(createRequest);

        return createdVacancy;
    }

    @ResponseBody
    @RequestMapping(value = "/vacancys", method = RequestMethod.PUT)
    public VacancyFullDto update(@RequestBody VacancyUpdateDto updateRequest) {

        VacancyFullDto updatedVacancy = vacancyService.update(updateRequest);


        return updatedVacancy;
    }

    @ResponseBody
    @RequestMapping(value = "/vacancys/{vacancyId}", method = RequestMethod.DELETE)
    public void delete(@PathVariable int id){

        vacancyService.deleteById(id);

    }

}
