package by.itstep.employment.controller;


import by.itstep.employment.dto.InterviewCreateDto;
import by.itstep.employment.dto.InterviewFullDto;
import by.itstep.employment.dto.InterviewShortDto;
import by.itstep.employment.dto.InterviewUpdateDto;
import by.itstep.employment.entity.InterviewEntity;
import by.itstep.employment.service.InterviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class InterviewController {

    @Autowired
    private InterviewService interviewService;

    @ResponseBody
    @RequestMapping(value = "/interviews", method = RequestMethod.GET)
    List<InterviewShortDto> findAll(){
        List<InterviewShortDto> allInterviews = interviewService.findAll();
        return allInterviews;
    }

    @ResponseBody
    @RequestMapping(value = "/interviews/{interviewId}", method = RequestMethod.GET)
    public InterviewFullDto findById(@PathVariable Integer id){
         InterviewFullDto interview = interviewService.findById(id);
         return interview;
    }

    @ResponseBody
    @RequestMapping(value = "/vacancys/{vacancyId}/users/{userId}/interviews", method = RequestMethod.POST)
    public InterviewFullDto create( @RequestBody  InterviewCreateDto createRequest){
       InterviewFullDto createdInterview = interviewService.create(createRequest);

       return createdInterview;
    }
    @ResponseBody
    @RequestMapping(value = "interviews/{status}/interviews/{interviewId}",method = RequestMethod.PUT)
    public InterviewFullDto update(@RequestBody InterviewUpdateDto updateRequest){
    InterviewFullDto updatedInterview = interviewService.updateStatus(updateRequest);
    return updatedInterview;
    }
}
