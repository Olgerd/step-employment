package by.itstep.employment.config;

import by.itstep.employment.entity.UserEntity;
import by.itstep.employment.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class SecurityService {

    @Autowired
    private UserRepository userRepository;

    public UserEntity getAuthenticatedUser(){
        String phone = SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getName();

        return userRepository.findByPhone(phone);

    }


}
