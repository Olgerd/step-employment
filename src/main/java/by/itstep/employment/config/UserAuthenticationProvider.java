package by.itstep.employment.config;

import by.itstep.employment.entity.UserEntity;
import by.itstep.employment.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;


@Component
public class UserAuthenticationProvider implements AuthenticationProvider {
    @Autowired
    private UserRepository userRepository;

    @Override
    public org.springframework.security.core.Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String phone = authentication.getName();
        String password = authentication.getCredentials().toString();

        System.out.println("Login : " + phone);
        System.out.println("Password: " + password);

        UserEntity foundUser = userRepository.findByPhone(phone);
        if (foundUser != null && foundUser.getPassword().equals(password)) {
            //Успех
            return new UsernamePasswordAuthenticationToken(phone, password, new ArrayList<>());
        } else {
            //Нет
            return null;
        }
    }

    @Override
    public boolean supports(Class<?> aClass) {

        return aClass.equals(UsernamePasswordAuthenticationToken.class);
    }
}