package by.itstep.employment.repository;

import by.itstep.employment.entity.VacancyEntity;
import by.itstep.employment.utils.EntityManagerUtils;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class VacancyHibernateRepository implements VacancyRepository{


    @Override
    public VacancyEntity findById(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();
        VacancyEntity foundVacancy = em.find(VacancyEntity.class, id);

        if(foundVacancy!=null) {
            Hibernate.initialize(foundVacancy.getInterviews());

        }
        em.getTransaction().commit();
        em.close();
        return foundVacancy;
    }

    @Override
    public List<VacancyEntity> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        List <VacancyEntity> allVacancys= em.createNativeQuery("SELECT * FROM vacancys", VacancyEntity.class).getResultList();

        for(VacancyEntity vacancy: allVacancys){
            Hibernate.initialize(vacancy.getInterviews());
        }

        em.getTransaction().commit();
        em.close();
        return allVacancys;
    }

    @Override
    public VacancyEntity create(VacancyEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();
        em.persist(entity);
        em.getTransaction().commit();
        em.close();
        return entity;
    }

    @Override
    public VacancyEntity update(VacancyEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        // todo merge
        VacancyEntity entityToUpdate = em.find(VacancyEntity.class, entity.getVacancyId());
        entityToUpdate.setName(entity.getName());
        entityToUpdate.setPosition(entity.getPosition());
        entityToUpdate.setSalary(entity.getSalary());
        entityToUpdate.setCompanyName(entity.getCompanyName());
        em.getTransaction().commit();
        em.close();
        return entityToUpdate;
    }

    @Override
    public void deleteById(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();
        VacancyEntity entityToDelete = em.find(VacancyEntity.class, id);
        em.remove(entityToDelete);
        em.getTransaction().commit();
        em.close();
    }
    @Override
    public void deleteAll(){
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM vacancys").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }



}
