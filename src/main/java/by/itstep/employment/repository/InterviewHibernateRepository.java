package by.itstep.employment.repository;

import by.itstep.employment.entity.InterviewEntity;
import by.itstep.employment.utils.EntityManagerUtils;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class InterviewHibernateRepository implements InterviewRepository{

    @Override
    public InterviewEntity findById(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        InterviewEntity foundInterview = em.find(InterviewEntity.class, id);
        if(foundInterview!=null) {
            Hibernate.initialize(foundInterview.getUser());
            Hibernate.initialize(foundInterview.getVacancy());
        }

        em.getTransaction().commit();
        em.close();
        return foundInterview;
    }

    @Override
    public List<InterviewEntity> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        List <InterviewEntity> allInterviews= em
                .createNativeQuery("SELECT * FROM interviews", InterviewEntity.class)
                .getResultList();

        em.getTransaction().commit();
        em.close();
        return allInterviews;
    }

    @Override
    public InterviewEntity create(InterviewEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(entity);

        em.getTransaction().commit();
        em.close();
        return entity;
    }

    @Override
    public InterviewEntity update(InterviewEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.merge(entity);

        em.getTransaction().commit();
        em.close();
        return entity;
    }

    @Override
    public void deleteById(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        InterviewEntity entityToDelete = em.find(InterviewEntity.class, id);
        em.remove(entityToDelete);

        em.getTransaction().commit();
        em.close();
    }
    @Override
    public void deleteAll(){
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM interviews").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }


}
