package by.itstep.employment.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;


import javax.persistence.*;
import java.sql.Date;


@Data
@Entity
@Table(name="interviews")
public class InterviewEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="interview_id")
    private Integer interviewId;

    @Column(name="status")
    private String status;

    @Column(name="date")
    private Date date;

    @ManyToOne
    @JoinColumn(name="user_id")
    private UserEntity user;

    @ManyToOne
    @JoinColumn(name="vacancy_id")
    private VacancyEntity vacancy;
}
