package by.itstep.employment.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name="vacancys")
public class VacancyEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "vacancy_id")
    private Integer vacancyId;

    @Column(name="name")
    private String name;

    @Column(name="position")
    private String position;

    @Column(name="salary")
    private Integer salary;

    @Column(name="company_name")
    private String companyName;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "vacancy", fetch = FetchType.LAZY)
    private List<InterviewEntity> interviews;
}
