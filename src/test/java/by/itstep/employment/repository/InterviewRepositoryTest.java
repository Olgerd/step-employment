package by.itstep.employment.repository;


import by.itstep.employment.entity.InterviewEntity;
import by.itstep.employment.entity.UserEntity;
import by.itstep.employment.entity.VacancyEntity;
import by.itstep.employment.utils.DatabaseCleaner;
import by.itstep.employment.utils.EntityUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class InterviewRepositoryTest {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private VacancyRepository vacancyRepository;
    @Autowired
    private InterviewRepository interviewRepository;
    @Autowired
    private DatabaseCleaner cleaner;

    @BeforeEach
    private void setUp(){

        cleaner.clean();
    }

    @Test
    public void create_happyPath(){
        //given
        UserEntity user = EntityUtils.prepareUser();
        userRepository.create(user);
        VacancyEntity vacancy = EntityUtils.prepareVacancy();
        vacancyRepository.create(vacancy);
        InterviewEntity interviewToCreate = EntityUtils.prepareInterview(vacancy, user);
        //when
        InterviewEntity createdInterview = interviewRepository.create(interviewToCreate);
        //then
        Assertions.assertNotNull(createdInterview.getInterviewId());
        InterviewEntity foundInterview = interviewRepository.findById(createdInterview.getInterviewId());
        Assertions.assertNotNull(foundInterview.getStatus());
        Assertions.assertEquals(interviewToCreate.getStatus(),foundInterview.getStatus());
        Assertions.assertEquals(user.getUserId(),foundInterview.getUser().getUserId());
        Assertions.assertEquals(vacancy.getVacancyId(),foundInterview.getVacancy().getVacancyId());
    }

    @Test
    public void findAll_happyPath(){
        //given
        UserEntity user = EntityUtils.prepareUser();
        VacancyEntity vacancy = EntityUtils.prepareVacancy();
        userRepository.create(user);
        vacancyRepository.create(vacancy);
        InterviewEntity interview1 = EntityUtils.prepareInterview(vacancy, user);
        InterviewEntity interview2 = EntityUtils.prepareInterview(vacancy, user);
        InterviewEntity interview3 = EntityUtils.prepareInterview(vacancy, user);
        interviewRepository.create(interview1);
        interviewRepository.create(interview2);
        interviewRepository.create(interview3);
        //when
        List<InterviewEntity> foundInterviews = interviewRepository.findAll();
        //then
        Assertions.assertEquals(3,foundInterviews.size());

    }
}
