package by.itstep.employment.repository;

import by.itstep.employment.entity.UserEntity;
import by.itstep.employment.utils.DatabaseCleaner;
import by.itstep.employment.utils.EntityUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.List;

@SpringBootTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private DatabaseCleaner cleaner;

    @BeforeEach
    public void setUp(){

        cleaner.clean();
    }

    @AfterEach
    public void clean(){
        cleaner.clean();
    }

    @Test
    public void create_happyPath(){
        //given
        UserEntity user = EntityUtils.prepareUser();
        System.out.println(user);

        //when
        UserEntity createdUser = userRepository.create(user);
        System.out.println(createdUser);
        //then
        Assertions.assertNotNull(createdUser.getUserId());
        UserEntity foundUser = userRepository.findById(createdUser.getUserId());
        Assertions.assertEquals(user.getFirstName(),foundUser.getFirstName());
        Assertions.assertEquals(user.getLastName(),foundUser.getLastName());
        Assertions.assertEquals(user.getPassword(),foundUser.getPassword());
        Assertions.assertEquals(user.getEmail(),foundUser.getEmail());
        Assertions.assertEquals(user.getPhone(),foundUser.getPhone());
        Assertions.assertEquals(user.getRole(),foundUser.getRole());
        Assertions.assertEquals(user.getYearsOfExperience(),foundUser.getYearsOfExperience());

    }

    @Test
    public void findAll(){
        //given

        //when
        List<UserEntity> foundUsers = userRepository.findAll();
        //then
        Assertions.assertTrue(foundUsers.isEmpty());
    }

    @Test
    public void findAll_happyPath(){
        //given
        addUserToDb();
        addUserToDb();
        //when
        List<UserEntity> foundUsers = userRepository.findAll();
        //then
        Assertions.assertEquals(2,foundUsers.size());
    }
    @Test
    public void update_happyPath(){
        //given
        UserEntity existingUser = addUserToDb();
        existingUser.setEmail("updated");
        existingUser.setPhone("updated");
        existingUser.setPassword("updated");
        //when
        UserEntity updatedUser = userRepository.update(existingUser);
        //then
        Assertions.assertEquals(existingUser.getUserId(), updatedUser.getUserId());
        UserEntity foundUser = userRepository.findById(existingUser.getUserId());
        Assertions.assertEquals(existingUser.getFirstName(),foundUser.getFirstName());
        Assertions.assertEquals(existingUser.getLastName(),foundUser.getLastName());
        Assertions.assertEquals(existingUser.getPassword(),foundUser.getPassword());
        Assertions.assertEquals(existingUser.getEmail(),foundUser.getEmail());
        Assertions.assertEquals(existingUser.getRole(),foundUser.getRole());
        Assertions.assertEquals(existingUser.getPhone(),foundUser.getPhone());
        Assertions.assertEquals(existingUser.getYearsOfExperience(),foundUser.getYearsOfExperience());
    }
    @Test
    public void delete_happyPath(){
        //given
        UserEntity existingUser = addUserToDb();
        //when
        userRepository.deleteById(existingUser.getUserId());
        //then
        UserEntity foundUser = userRepository.findById(existingUser.getUserId());
        Assertions.assertNull(foundUser);
    }


    private UserEntity addUserToDb(){
        UserEntity userToAdd = EntityUtils.prepareUser();
        return userRepository.create(userToAdd);
    }


}
