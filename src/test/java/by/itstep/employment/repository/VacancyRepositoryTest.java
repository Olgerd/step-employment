package by.itstep.employment.repository;


import by.itstep.employment.entity.UserEntity;
import by.itstep.employment.entity.VacancyEntity;
import by.itstep.employment.utils.DatabaseCleaner;
import by.itstep.employment.utils.EntityUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.List;

@SpringBootTest
public class VacancyRepositoryTest {

    @Autowired
    private VacancyRepository vacancyRepository;
    @Autowired
    private DatabaseCleaner cleaner;

    @BeforeEach
    public void setUp(){

        cleaner.clean();
    }

    @AfterEach
    public void clean(){
        cleaner.clean();
    }

    @Test
    public void create_happyPath(){
        //given
        VacancyEntity vacancy = EntityUtils.prepareVacancy();
        System.out.println(vacancy);
        //when
        VacancyEntity createdVacancy = vacancyRepository.create(vacancy);
        //then
        Assertions.assertNotNull(createdVacancy.getVacancyId());
        VacancyEntity foundVacancy = vacancyRepository.findById(createdVacancy.getVacancyId());
        Assertions.assertEquals(vacancy.getName(),foundVacancy.getName());
        Assertions.assertEquals(vacancy.getPosition(),foundVacancy.getPosition());
        Assertions.assertEquals(vacancy.getSalary(),foundVacancy.getSalary());
        Assertions.assertEquals(vacancy.getCompanyName(),foundVacancy.getCompanyName());

    }

    @Test
    public void findAll_whenNoOneFound(){
        //given
        //when
        List<VacancyEntity> foundVacancys = vacancyRepository.findAll();
        //then
        Assertions.assertTrue(foundVacancys.isEmpty());
    }

    @Test
    public void findAll_happyPath(){
        //given
        addVacancyToDb();
        addVacancyToDb();
        //when
        List<VacancyEntity> foundVacancys = vacancyRepository.findAll();
        //then
        Assertions.assertEquals(2,foundVacancys.size());
    }
    @Test
    public void update_happyPath(){
        //given
        VacancyEntity existingVacancy = addVacancyToDb();
        existingVacancy.setName("updated");
        existingVacancy.setCompanyName("updated");
        existingVacancy.setPosition("updated");
        //when
        VacancyEntity updatedVacancy = vacancyRepository.update(existingVacancy);
        //then
        Assertions.assertEquals(existingVacancy.getVacancyId(), updatedVacancy.getVacancyId());
        VacancyEntity foundVacancy = vacancyRepository.findById(existingVacancy.getVacancyId());
        Assertions.assertEquals(existingVacancy.getName(),foundVacancy.getName());
        Assertions.assertEquals(existingVacancy.getCompanyName(),foundVacancy.getCompanyName());
        Assertions.assertEquals(existingVacancy.getPosition(),foundVacancy.getPosition());
        Assertions.assertEquals(existingVacancy.getSalary(),foundVacancy.getSalary());

    }
    @Test
    public void delete_happyPath(){
        //given
        VacancyEntity existingVacancy = addVacancyToDb();
        //when
         vacancyRepository.deleteById(existingVacancy.getVacancyId());
        //then
        VacancyEntity foundVacancy = vacancyRepository.findById(existingVacancy.getVacancyId());
        Assertions.assertNull(foundVacancy);
    }


    private VacancyEntity addVacancyToDb(){
        VacancyEntity vacancyToAdd = EntityUtils.prepareVacancy();
        return vacancyRepository.create(vacancyToAdd);
    }


}
