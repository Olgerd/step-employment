package by.itstep.employment.utils;


import by.itstep.employment.entity.InterviewEntity;
import by.itstep.employment.entity.UserEntity;
import by.itstep.employment.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class DatabaseCleaner {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private VacancyRepository vacancyRepository;
    @Autowired
    private InterviewRepository interviewRepository;



    public void clean(){
        userRepository = new UserHibernateRepository();
        vacancyRepository = new VacancyHibernateRepository();
        interviewRepository = new InterviewHibernateRepository();


        for(InterviewEntity interview : interviewRepository.findAll()){            //чтобы таблица many-to-one стала пустой
            System.out.println(interview);
            interviewRepository.update(interview);
        }


        interviewRepository.deleteAll();
        vacancyRepository.deleteAll();
        userRepository.deleteAll();
    }

}
