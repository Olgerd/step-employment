package by.itstep.employment.utils;

import by.itstep.employment.entity.InterviewEntity;
import by.itstep.employment.entity.UserEntity;
import by.itstep.employment.entity.VacancyEntity;

import java.sql.Date;
import java.time.LocalDateTime;

public class EntityUtils {

    public static UserEntity prepareUser(){
        UserEntity user = new UserEntity();

        user.setRole("Role#"+"CANDIDATE");
        user.setFirstName("FirstName#"+Math.random());
        user.setLastName("LastName#"+Math.random());
        user.setPassword("Pass"+Math.random());
        user.setEmail(Math.random()+"@gmail.com");
        user.setPhone("Phone#"+Math.random());
        user.setPosition("Position#"+ "QA");
        user.setYearsOfExperience(1 + 10*(int)Math.random());
        return user;
    }

    public static VacancyEntity prepareVacancy(){
        VacancyEntity vacancy = new VacancyEntity();

        vacancy.setName("Name#" + Math.random());
        vacancy.setPosition("Position#"+ "QA");
        vacancy.setSalary(1 + 10*(int)Math.random());
        vacancy.setCompanyName("CompanyName" + Math.random());
        return vacancy;
    }

    public static InterviewEntity prepareInterview(VacancyEntity vacancy, UserEntity user){
        InterviewEntity interview = new InterviewEntity();

        interview.setStatus("Status#"+ Math.random());
        Date date = new Date(2021-12-30);
        interview.setDate(date);
        interview.setUser(user);
        interview.setVacancy(vacancy);
        return interview;
    }


}
