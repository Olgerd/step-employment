package by.itstep.employment.service;


import by.itstep.employment.dto.InterviewCreateDto;
import by.itstep.employment.dto.InterviewFullDto;
import by.itstep.employment.entity.InterviewEntity;
import by.itstep.employment.entity.UserEntity;
import by.itstep.employment.entity.VacancyEntity;
import by.itstep.employment.mapper.InterviewMapper;
import by.itstep.employment.repository.InterviewRepository;
import by.itstep.employment.repository.UserRepository;
import by.itstep.employment.repository.VacancyRepository;
import by.itstep.employment.utils.DatabaseCleaner;
import by.itstep.employment.utils.EntityUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class InterviewServiceTest {

    @Autowired
    private DatabaseCleaner dbCleaner;
    @Autowired
    private InterviewService interviewService;
    @Autowired
    private VacancyRepository vacancyRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private InterviewRepository interviewRepository;
    @Autowired
    private InterviewMapper interviewMapper;

    @BeforeEach
    public void setUp(){

        dbCleaner.clean();
    }

    @Test
    public void byId_happyPath(){
        //given
        UserEntity user = EntityUtils.prepareUser();
        userRepository.create(user);
        VacancyEntity vacancy = EntityUtils.prepareVacancy();
        vacancyRepository.create(vacancy);

        InterviewEntity interview = EntityUtils.prepareInterview(vacancy,user);
        interviewRepository.create(interview);
        interview.setVacancy(vacancy);
        interviewRepository.update(interview);
        //when
        InterviewFullDto dto = interviewService.findById(interview.getInterviewId());
        interviewMapper.map(dto);
        //then\
        Assertions.assertNotNull(interviewRepository.findById(interview.getInterviewId()));

    }

}
