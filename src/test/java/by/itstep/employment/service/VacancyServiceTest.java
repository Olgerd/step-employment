package by.itstep.employment.service;


import by.itstep.employment.entity.InterviewEntity;
import by.itstep.employment.entity.UserEntity;
import by.itstep.employment.entity.VacancyEntity;
import by.itstep.employment.repository.InterviewRepository;
import by.itstep.employment.repository.UserRepository;
import by.itstep.employment.repository.VacancyRepository;
import by.itstep.employment.utils.DatabaseCleaner;
import by.itstep.employment.utils.EntityUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;

@SpringBootTest
public class VacancyServiceTest {

    @Autowired
    private DatabaseCleaner dbCleaner;
    @Autowired
    private VacancyService vacancyService;
    @Autowired
    private VacancyRepository vacancyRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private InterviewRepository interviewRepository;

    @BeforeEach
    public void setUp(){

        dbCleaner.clean();
    }

    @Test
    public void deleteById_happyPath(){
        //given
        UserEntity user = EntityUtils.prepareUser();
        userRepository.create(user);
        VacancyEntity vacancy = EntityUtils.prepareVacancy();
        vacancyRepository.create(vacancy);

        InterviewEntity interview = EntityUtils.prepareInterview(vacancy,user);
        interviewRepository.create(interview);
        interview.setVacancy(vacancy);
        interviewRepository.update(interview);
        //when
        vacancyService.deleteById(vacancy.getVacancyId());
        //then
        Assertions.assertNotNull(userRepository.findById(user.getUserId()));
        Assertions.assertNull(interviewRepository.findById(interview.getInterviewId()));
        Assertions.assertNull(vacancyRepository.findById(vacancy.getVacancyId()));
    }

}
